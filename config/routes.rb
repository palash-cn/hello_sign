Rails.application.routes.draw do
  resources :documents do
    get :e_sign, on: :member
  end
  root "home#index"
end
