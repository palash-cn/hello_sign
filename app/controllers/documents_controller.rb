class DocumentsController < ApplicationController
  before_action :set_document, only: [:show, :edit, :update, :destroy, :e_sign]

  def index
    @documents = Document.all
  end

  def show
  end

  def new
    @document = Document.new
  end

  def edit
  end

  def create
    @document = Document.new(document_params)

    if @document.save
      redirect_to @document, notice: 'Document was successfully created.'
    else
      render "new"
    end
  end

  def update
    if @document.update(document_params)
      redirect_to @document, notice: 'Document was successfully updated.'
    else
      render "edit"
    end
  end

  def destroy
    @document.destroy
    redirect_to documents_path, notice: 'Document was successfully destroyed.'
  end

  def e_sign
    client = HelloSign::Client.new :api_key => '658d53b22fc04533f913ff2c2096373009ecfb1285447b958eab53b45983ee5c'
    client.create_embedded_signature_request({
      test_mode: 1,
      client_id: '8e743f6d6245c07a1b907741b543fb14',
      title: 'NDA with Acme Co.',
      subject: 'The NDA we talked about',
      message: 'Please sign this NDA and then we can discuss more. Let me know if you have any questions.',
      signers: [
        {
          email_address: 'palash.bera@capitalnumbers.com',
          name: 'Palash Bera'
        }
      ],
      attachments: [{
        name: 'NDA',
        required: true
        }
      ],
      files: ['NDA.pdf']
    })
  end

  private
    def set_document
      @document = Document.find(params[:id])
    end

    def document_params
      params.require(:document).permit(:name, :file)
    end
end
